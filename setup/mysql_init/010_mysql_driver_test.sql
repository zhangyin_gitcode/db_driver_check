CREATE DATABASE mysql_driver_test;

USE mysql_driver_test;

CREATE TABLE `mysql_driver_test` (
    `id`   int NOT NULL AUTO_INCREMENT,
    `name` varchar(255),
    PRIMARY KEY (`id`)
) ENGINE = InnoDB;

INSERT INTO `mysql_driver_test` (`name`) VALUES ('test1');