# setup

此文件夹中的文件用于设置单元测试环境。

## 依赖

+ Docker Compose

## 启动

```
# cd to [project_root]/setup
docker compose up -d
```

## 停止

```
# cd to [project_root]/setup
docker compose down --volumes
```